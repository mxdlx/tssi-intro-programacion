Proceso EsTriangulo
	Definir lado1, lado2, lado3 como Entero;

	Mostrar "Ingrese la longitud del primer lado: ";
	Leer lado1;

	Mostrar "Ingrese la longitud del segundo lado: ";
	Leer lado2;

	Mostrar "Ingrese la longitud del tercer lado: ";
	Leer lado3;

	// TODO: guardar el resultado logico en una variable y no repetir los mensajes
	Si (lado1 + lado2 > lado3) Entonces
		Si (lado2 + lado3 > lado1) Entonces
			Si (lado3 + lado1 > lado2) Entonces
				Mostrar "Es un triangulo.";
			Sino
				Mostrar "No es triangulo";
			FinSi
		Sino
			Mostrar "No es triangulo.";
		FinSi
	Sino
		Mostrar "No es triangulo.";
	FinSi
FinProceso
