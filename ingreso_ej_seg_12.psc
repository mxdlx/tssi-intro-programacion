Proceso Sueldo
	Definir sueldoNeto, sueldoEmpleado, porcentaje como Real;
	Definir hijos, antiguedad, bono como Entero;
	Definir categoria como Caracter;

	Mostrar "Ingrese sueldo neto: ";
	Leer sueldoNeto;

	Mostrar "Ingrese cantidad de hijos: ";
	Leer hijos;

	Mostrar "Ingrese anios de antiguedad: ";
	Leer antiguedad;

	Mostrar "Ingrese categoria (_C_adete, _A_dministrativo, _S_upervisor, _G_erente): ";
	Leer categoria;

	Segun categoria
		'C': bono = 0;
		'A': bono = 3000;
		'S': bono = 6000;
		'G': bono = 10000;
	FinSegun

	// La antiguedad es un numero entero para este caso
	Si (antiguedad > 10) Entonces
		porcentaje = 1;
	Sino
		Si (antiguedad > 4) Entonces
			porcentaje = 0.6;
		Sino
			Si (antiguedad > 2) Entonces
				porcentaje = 0.25;
			Sino
				porcentaje = 0;
			FinSi
		FinSi
	FinSi

	sueldoEmpleado = (sueldoNeto + bono + 350*hijos) * (1 + porcentaje);
	
	Mostrar sueldoEmpleado;
FinProceso
