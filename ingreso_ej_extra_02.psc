Proceso StatsLluvia
	Definir lluviaS1, lluviaS2, lluviaS3, lluviaS4, promedioS1, promedioS2, promedioS3, promedioS4 como Real;

	Mostrar "Ingrese milimetros cubicos de la primera semana: ";
	Leer lluviaS1;

	Mostrar "Ingrese milimetros cubicos de la segunda semana: ";
	Leer lluviaS2;

	Mostrar "Ingrese milimetros cubicos de la tercera semana: ";
	Leer lluviaS3;

	Mostrar "Ingrese milimetros cubicos de la cuarta semana: ";
	Leer lluviaS4;

	promedioS1 = lluviaS1 / 7;
	promedioS2 = lluviaS2 / 7;
	promedioS3 = lluviaS3 / 7;
	promedioS4 = lluviaS4 / 7;

	Mostrar "Promedio diario semana 1: ", promedioS1, "mm cubico/s por dia.";
	Mostrar "Promedio diario semana 2: ", promedioS2, "mm cubico/s por dia.";
	Mostrar "Promedio diario semana 3: ", promedioS3, "mm cubico/s por dia.";
	Mostrar "Promedio diario semana 4: ", promedioS4, "mm cubico/s por dia.";

	Si (promedioS1 > promedioS2) Entonces
		Si (promedioS1 > promedioS3) Entonces
			Si (promedioS1 > promedioS4) Entonces
				Mostrar "El mayor promedio diario se dio en la semana 1.";
			Sino
				Mostrar "El mayor promedio diario se dio en la semana 4.";
			FinSi
		Sino
			Si (promedioS3 > promedioS4) Entonces
				Mostrar "El mayor promedio diario se dio en la semana 3.";
			Sino
				Mostrar "El mayor promedio diario se dio en la semana 4.";
			FinSi
		FinSi
	Sino
		Si (promedioS2 > promedioS3) Entonces
			Si (promedioS2 > promedioS4) Entonces
				Mostrar "El mayor promedio se dio en la semana 2.";
			Sino
				Mostrar "El mayor promedio se dio en la semana 4.";
			Finsi
		Sino
			Si (promedioS3 > promedioS4) Entonces
				Mostrar "El mayor promedio diario se dio en la semana 3.";
			Sino
				Mostrar "El mayor promedio diario se dio en la semana 4.";
			FinSi
		FinSi
	FinSi

	Mostrar "El total caido fue ", ((lluviaS1 + lluviaS2 + lluviaS3 + lluviaS4) / 1000), "cm cubicos.";
FinProceso
