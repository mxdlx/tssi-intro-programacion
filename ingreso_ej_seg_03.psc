Proceso FormatoHora
	Definir horario24, hora24, hora12, minutos como Entero;

	Mostrar "Ingrese una hora en Formato 24 sin separador (HHMM): ";
	Leer horario24;

	hora24 = trunc(horario24 / 100);
	minutos = horario24 - (hora24 * 100);

	Si (hora24 == 0) Entonces
		Mostrar "La hora es ", 12, ":", minutos, " AM.";
	FinSi
	Si (hora24 > 0 & hora24 < 12) Entonces
		Mostrar "La hora es ", hora24, ":", minutos, " AM.";
	FinSi
	Si (hora24 == 12) Entonces
		Mostrar "La hora es ", hora24, ":", minutos, " PM.";
	FinSi
	Si (hora24 > 12 & hora24 <= 23) Entonces
		hora12 = hora24 % 12;
		Mostrar "La hora es ", hora12, ":", minutos, " PM.";
	FinSi
FinProceso
