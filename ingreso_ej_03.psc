Proceso GeometriaCuadrado
	Definir lado, area, perimetro como Real;

	Mostrar "Ingrese el lado del cuadrado: ";
	Leer lado;

	area = lado * lado;
	perimetro = lado * 4;

	Mostrar "El area del cuadrado es ", area;
	Mostrar "El perimetro del cuadrado es ", perimetro;
FinProceso
