Proceso TrianguloRectangulo
	Definir lado1, lado2, lado3 como Entero;

	Mostrar "Ingrese el primer lado (Cateto 1): ";
	Leer lado1;

	Mostrar "Ingrese el segundo lado (Cateto 2): ";
	Leer lado2;

	Mostrar "Ingrese el tercer lado (Hipotenusa): ";
	Leer lado3;

	Si (lado1^2 + lado2^2 = lado3^2) Entonces
		Mostrar "El triangulo es rectangulo.";
	Sino
		Mostrar "El triangulo no es rectangulo.";
	FinSi
FinProceso
