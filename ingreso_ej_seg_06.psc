Proceso AnioBisiesto
	Definir anio como Entero;

	Mostrar "Ingrese un anio: ";
	Leer anio;

	Si ((anio % 4 == 0 & ~(anio % 100 == 0)) | anio % 400 == 0) Entonces
		Mostrar "El anio es bisiesto.";
	Sino
		Mostrar "El anio no es bisiesto";
	FinSi
FinProceso
