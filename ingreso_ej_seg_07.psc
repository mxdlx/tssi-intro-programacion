Proceso MesAnio
	Definir mes, anio, diasMes, diasFebrero como Entero;

	Mostrar "Ingrese un anio: ";
	Leer anio;

	Si ((anio % 4 == 0 & ~(anio % 100 == 0)) | anio % 400 == 0) Entonces
		diasFebrero = 29;
	Sino
		diasFebrero = 28;
	FinSi

	Mostrar "Ingrese un mes: ";
	Leer mes;

	Segun mes
		1, 3, 5, 7, 8, 10, 12: diasMes = 31;
		2: diasMes = diasFebrero;
		4, 6, 9, 11: diasMes = 30;
	FinSegun

	Mostrar "El mes tiene ", diasMes, " dias.";

FinProceso
