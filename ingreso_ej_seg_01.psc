Proceso PromedioAlumno
	Definir nota1, nota2, nota3 como Entero;
	Definir promedio como Real;

	Mostrar "Ingrese la primera nota: ";
	Leer nota1;

	Mostrar "Ingrese la segunda nota: ";
	Leer nota2;

	Mostrar "Ingrese la ultima nota: ";
	Leer nota3;

	promedio = (nota1 + nota2 + nota3) / 3;

	Si (promedio > 8) Entonces
		Mostrar "El promedio es Sobresaliente.";
	Sino
		Si (promedio > 7) Entonces
			Mostrar "El promedio esta Muy Bien.";
		Sino
			Si (promedio > 5) Entonces
				Mostrar "El promedio esta Bien.";
			Sino
				Si (promedio > 3) Entonces
					Mostrar "El promedio es Regular.";
				Sino
					Mostrar "El promedio es Insuficiente.";
				FinSi
			FinSi
		FinSi
	FinSi
FinProceso
