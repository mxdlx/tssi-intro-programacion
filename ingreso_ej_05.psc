Proceso FechaFormato
	Definir dia, mes, anio, mesOffset, anioOffset, fecha como Entero;

	Mostrar "Ingrese el dia: ";
	Leer dia;

	Mostrar "Ingrese el mes: ";
	Leer mes;

	Mostrar "Ingrese el anio: ";
	Leer anio;

	anioOffset = anio * 10000;
	mesOffset = mes * 100;

	fecha = anioOffset + mesOffset + dia;

	Mostrar "La fecha es ", fecha;
Fin Proceso
