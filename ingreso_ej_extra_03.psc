Proceso OperacionBinaria
	Definir numeroA, numeroB como Entero;
	Definir operacion como Entero;

	Mostrar "Ingrese primer numero: ";
	Leer numeroA;

	Mostrar "Ingrese segundo numero: ";
	Leer numeroB;

	Mostrar "Ingrese numero de operacion (1: + | 2: - | 3: / | 4: *): ";
	Leer operacion;

	Segun operacion
		1: Mostrar "La operacion N1 + N2 es: ", (numeroA + numeroB), ".";
		2: Mostrar "La operacion N1 - N2 es: ", (numeroA - numeroB), ".";
		3:
			Si (numeroB != 0) Entonces
				Mostrar "La operacion N1 / N2 es: ", (numeroA / numeroB), ".";
			Sino
				Mostrar "No se puede dividir por cero.";
			FinSi
		4: Mostrar "La operacion N1 * N2 es: ", (numeroA * numeroB), ".";
	FinSegun
FinProceso
